this repository will build [shellinabox](https://github.com/shellinabox/shellinabox) and [webconsole](http://www.web-console.org/)

Build the container

```
#!python

docker-compose build
docker-compose up
```

you can access webconsole on your localhost

```
#!python

http://localhost/
```
username is root and password is root. both these values can be changed in the index.php

shell -in-a-box can be accessed on port 4200

```
#!python

http://localhost:4200/
```
by default you will be logged in as username.

for more information read the article [here](https://softint.atlassian.net/wiki/display/CSP/CSP-4337+web+based+terminal+to+run+Job+processing+from+web+browser).