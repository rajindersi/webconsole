#!/bin/bash

#ssh config to be used for shellinabox
mkdir /var/run/sshd
chmod 0755 /var/run/sshd
/usr/sbin/sshd

useradd --create-home --shell /bin/bash --groups sudo username ## includes 'sudo'

# start Apache
service apache2 start

#start shel in a box without using https for user - username group - username
shellinaboxd -t -s /:username:username:/:bash