FROM ubuntu:trusty
MAINTAINER Rajinder Deol <rajinders@softint.com.au>

# Install base packages
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -yq install \
        openssh-server \
        shellinabox \
        ssh \
        curl \
        apache2 \
        libapache2-mod-php5 \
        php-pear

# Update the default apache site with the config.
COPY docker/apache2/config/apache-config.conf /etc/apache2/sites-enabled/000-default.conf

# start apache and shellinabox
ENTRYPOINT /var/www/webconsole/docker/shellinabox/start.sh